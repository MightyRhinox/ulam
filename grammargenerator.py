# -*- coding: utf-8 -*-
"""
Created on Sun May  6 21:09:58 2018

@author: roder
"""


class GrammarRule:
    def __init__(self, name, rule):
        self.name = name
        self.rule = rule
        
class Grammar:
    def __init__(self, name, language):
        self.name = name
        self.language = language
        self.grammar = []
        
    def add_rule(self, rule):
        self.grammar.append(rule)
        