# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 18:01:23 2018

@author: roder
"""

import os
from nltk.corpus import PlaintextCorpusReader


path = os.path.expanduser('~/nltk_data/gutenbergtext')
test = PlaintextCorpusReader(path, '.*\.txt')

sents = test.sents()
sents = [sent for sent in sents]