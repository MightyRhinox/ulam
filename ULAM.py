# -*- coding: utf-8 -*-
"""
Created on Sun May  6 20:38:04 2018

@author: roder
"""

import lexeme
import grammargenerator
import nltk
import re

from nltk.corpus import brown

browntext = brown.sents()
Lexeme = lexeme.Lexeme
Lexicon = lexeme.Lexicon
GrammarRule = grammargenerator.GrammarRule
Grammar = grammargenerator.Grammar
reword = '([A-Z]|[a-z]*)'

E = lexeme.EADClass.E
A = lexeme.EADClass.A
D = lexeme.EADClass.D

brownsents = ''

for sent in browntext:
    brownsents += (nltk.tokenize.treebank.TreebankWordDetokenizer().detokenize(sent))

StdEnglishLex = Lexicon('Standard', 'English')
StdEnglishGram = Grammar('Standard', 'English')
newWord = Lexeme('I', E)
newRule = GrammarRule('simpleSV', (lambda E, A: '({} {}\.)'.format(E, A)))

newWord.add_function(newRule.name)

StdEnglishLex.add_lexeme(newWord)
StdEnglishGram.add_rule(newRule)

acquisition_rule = re.compile(newRule.rule('I', reword))
acquired = re.findall(acquisition_rule, brownsents)
    
for lex in acquired:
    if StdEnglishLex.check_lexeme_exist(lex[1]) == False:
        newWord = Lexeme(lex[1], A)
        newWord.add_function(newRule.name)
        StdEnglishLex.add_lexeme(newWord)

EntMorphs = StdEnglishLex.get_morph_from_role(E)
print("Entities in lexicon: {}".format(EntMorphs))

ActMorphs = StdEnglishLex.get_morph_from_role(A)
print("Actors in lexicon: {}".format(ActMorphs))

SVMorphs = StdEnglishLex.get_morph_from_rule(newRule.name)
print("Morphs that can be used in the rule {}: {}".format(newRule.name, SVMorphs))