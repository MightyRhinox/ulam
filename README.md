# ULAM - The Universal Language Acquisition Model
The Universal Language Acquisition Model is an attempt to construct a
Semi-Supervised Natural Language Acquistion Tool.
This work is being done as an honours thesis for the Bachelor Of Engineering - Computer And Software Systems at
the Queensland University of Technology, Brisbane Australia.

The work is based on the idea of using a statically constructed, strictly formalised Grammar to filter a corpus
and then use a variety of methods to identify patterns of language use that may constitute examples of different
lects or variants of the natural language.

The script uses elements of NLTK and was written using a large untagged corpus from the Gutenberg Project.
This is still very much a work in progress.

Roderick Lenz
