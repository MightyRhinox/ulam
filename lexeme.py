# -*- coding: utf-8 -*-
"""
Created on Sun May  6 19:48:01 2018

@author: roder
"""
  
class Lexeme:
    def __init__(self, morph):
        self.morph = morph
        self.functions = []

    def add_function(self, function):
        self.functions.append(function)

class Entity(Lexeme):
    pass

class Act(Lexeme):
    pass

class Descriptor(Lexeme):
    pass
        
class Lexicon:
    def __init__(self, name, language):
        self.language = language
        self.lexicon = []
    
    def add_lexeme(self, lexeme):
        self.lexicon.append(lexeme)
        
    def get_morph_from_role(self, role):
        morphList = []
        for lex in self.lexicon:
            if type(lex) is role: morphList.append(lex.morph)
        return morphList
    
    def get_morph_from_rule(self, grule):
        morphList = []
        for lex in self.lexicon:
            if grule.name in lex.functions: morphList.append(lex.morph)
        return morphList
    
    def check_lexeme_exist(self, morph):
        for lex in self.lexicon:
            if lex.morph == morph:
                return True
        
        return False